package com.almundo.callcenter.repository.employee;

import com.almundo.callcenter.domain.model.employee.Employee;

import java.util.List;

public interface EmployeeRepository {
    void addEmployee(Employee employee);
    void deleteEmployee(int employeeId);
    List<Employee> getEmployees();
}
