package com.almundo.callcenter.repository.employee.impl;

import com.almundo.callcenter.domain.model.employee.Employee;
import com.almundo.callcenter.repository.employee.EmployeeRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {
    private List<Employee> employees;

    public EmployeeRepositoryImpl() {
        employees = new ArrayList<>();
    }


    @Override
    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    @Override
    public void deleteEmployee(int employeeId) {
        Employee employee = employees.stream().filter(x -> x.getId() == employeeId).findFirst().get();
        employees.remove(employee);
    }

    @Override
    public List<Employee> getEmployees() {
        return employees;
    }
}
