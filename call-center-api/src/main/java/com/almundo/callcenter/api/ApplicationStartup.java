package com.almundo.callcenter.api;

import com.almundo.callcenter.domain.model.employee.Employee;
import com.almundo.callcenter.service.employee.EmployeeService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    EmployeeService employeeService;

    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        loginEmployeeFromResources();
    }

    public void loginEmployeeFromResources(){
        final String ENCODING = "UTF-8";
        List<Employee> employees = null;
        try {
            ObjectMapper objectMapper = new ObjectMapper();

            InputStream inputStream = new ClassPathResource("employees.json").getInputStream();
            String result = IOUtils.toString(inputStream, ENCODING);

            employees = objectMapper.readValue(result, new TypeReference<List<Employee>>(){});

            logger.info(result);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Employee employee : employees) {
            employeeService.login(employee);
        }
    }
}
