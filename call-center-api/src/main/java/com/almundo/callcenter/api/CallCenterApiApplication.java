package com.almundo.callcenter.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@ComponentScan ({"com.almundo.callcenter.api", "com.almundo.callcenter.service", "com.almundo.callcenter.repository"})
public class CallCenterApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CallCenterApiApplication.class, args);
	}
}
