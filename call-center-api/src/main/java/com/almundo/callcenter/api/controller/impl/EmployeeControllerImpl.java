package com.almundo.callcenter.api.controller.impl;

import com.almundo.callcenter.api.controller.EmployeeController;
import com.almundo.callcenter.api.controller.model.Response;
import com.almundo.callcenter.domain.model.employee.*;
import com.almundo.callcenter.service.employee.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/callcenter/employee")
public class EmployeeControllerImpl implements EmployeeController{

    private final String OK_MESSAGE = "OK";

    @Autowired
    private EmployeeService employeeService;

    @Override
    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public Response login(@RequestBody Employee employee) {
        Response response = new Response();
        try{
            employeeService.login(employee);

            response.setMessage(OK_MESSAGE);
        }catch (Exception e){
            response.setMessage(e.getMessage());
        }

        return response;
    }

    @Override
    @RequestMapping(path = "/logout/{employeeId}", method = RequestMethod.GET)
    public Response logout(@PathVariable("employeeId")int employeeId) {
        Response response = new Response();
        try{
            employeeService.logout(employeeId);

            response.setMessage(OK_MESSAGE);
        }catch (Exception e){
            response.setMessage(e.getMessage());
        }

        return response;
    }

    @Override
    @RequestMapping(path = "/getAllEmployees", method = RequestMethod.GET)
    public List<Employee> getAllEmployees() {
        return employeeService.getAllEmployees();
    }

    @Override
    @RequestMapping(path = "/getOnlineEmployee", method = RequestMethod.GET)
    public Employee getOnlineEmployee() {
        return employeeService.getOnlineEmployee();
    }
}
