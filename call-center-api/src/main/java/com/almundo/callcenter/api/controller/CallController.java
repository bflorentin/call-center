package com.almundo.callcenter.api.controller;


import com.almundo.callcenter.domain.model.Call;

import java.util.List;

public interface CallController {
    List<Call> call(List<Call> calls);
}
