package com.almundo.callcenter.api.controller.impl;

import com.almundo.callcenter.api.controller.CallController;
import com.almundo.callcenter.domain.model.Call;
import com.almundo.callcenter.domain.model.Customer;
import com.almundo.callcenter.service.call.CallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/callcenter/call")
public class CallControllerImpl implements CallController {

    @Autowired
    private CallService callService;

    @Override
    @RequestMapping(path = "/", method = RequestMethod.POST)
    public List<Call> call(@RequestBody List<Call> calls) {
        try {
            callService.processCalls(calls);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return calls;
    }
}
