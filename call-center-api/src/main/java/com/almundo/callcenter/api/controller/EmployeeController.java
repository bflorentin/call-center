package com.almundo.callcenter.api.controller;

import com.almundo.callcenter.api.controller.model.Response;
import com.almundo.callcenter.domain.model.employee.Employee;

import java.util.List;

public interface EmployeeController {
    Response login(Employee employee);
    Response logout(int employeeId);
    List<Employee> getAllEmployees();

    Employee getOnlineEmployee();
}
