package com.almundo.callcenter.api.controller.model;

public class Response<T> {
    private String message;
    private T responseBody;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
