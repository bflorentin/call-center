package com.almundo.callcenter.service.threading;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class CallsExecutor {
    @Value("${calls.thread.core-pool}")
    private int corePoolSize;

    @Value("${calls.thread.max-pool}")
    private int maxPoolSize;

    @Value("${calls.queue.capacity}")
    private int queueCapacity;

    @Value("${calls.thread.timeout}")
    private int threadTimeout;

    @Value("${calls.thread.thread-name-prefix}")
    private String threadNamePrefix;

    @Bean
    @Primary
    @Qualifier("callsExecutor")
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(corePoolSize);
        threadPoolTaskExecutor.setMaxPoolSize(maxPoolSize);
        threadPoolTaskExecutor.setQueueCapacity(queueCapacity);
        threadPoolTaskExecutor.setKeepAliveSeconds(threadTimeout);
        threadPoolTaskExecutor.setThreadNamePrefix(threadNamePrefix);

        return threadPoolTaskExecutor;
    }
}
