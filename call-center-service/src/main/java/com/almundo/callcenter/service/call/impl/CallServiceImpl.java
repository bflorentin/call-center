package com.almundo.callcenter.service.call.impl;

import com.almundo.callcenter.domain.model.Call;
import com.almundo.callcenter.service.call.CallDispatcher;
import com.almundo.callcenter.service.call.CallService;
import com.almundo.callcenter.service.dto.CallProcessResultDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Service
public class CallServiceImpl implements CallService {

    @Autowired
    private CallDispatcher callDispatcher;

    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Override
    public void processCalls(List<Call> calls) throws InterruptedException, ExecutionException {

        List<Future<CallProcessResultDto>> callResults = new ArrayList<>();

        for (Call call : calls) {
            Future<CallProcessResultDto> process = callDispatcher.dispatchCall(call);
            callResults.add(process);
        }
        // Espero a que todas las llamadas sean procesadas
        // Si todavía no terminó espero 2seg para checkear nuevamente
        while(callResults.stream().filter(x -> !x.isDone()).collect(Collectors.toList()).size() > 0){
            Thread.sleep(2000);
        }
        logger.info("** All Processes are DONE!");

        for (Future<CallProcessResultDto> result : callResults) {
            logger.info(result.get().getResultMessage());
        }
    }
}
