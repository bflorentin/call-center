package com.almundo.callcenter.service.call;

import com.almundo.callcenter.domain.model.Call;

import java.util.List;
import java.util.concurrent.ExecutionException;

public interface CallService {
    /**
     * Procesa una lista de llamadas en paralelo (dependiendo de los empleados activos).
     * @param calls llamadas a procesar.
     */
    void processCalls(List<Call> calls) throws InterruptedException, ExecutionException;
}
