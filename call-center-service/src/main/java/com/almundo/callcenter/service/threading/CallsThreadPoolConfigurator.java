package com.almundo.callcenter.service.threading;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

@Component
public class CallsThreadPoolConfigurator {
    @Value("${calls.thread.core-pool}")
    private int corePoolSize;

    @Value("${calls.thread.max-pool}")
    private int maxPoolSize;

    @Autowired
    @Qualifier("callsExecutor")
    ThreadPoolTaskExecutor threadPoolTaskExecutor;

    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    public void increasePoolSize(){
        int executorPoolSize = threadPoolTaskExecutor.getCorePoolSize();

        if (executorPoolSize < corePoolSize){
            threadPoolTaskExecutor.setCorePoolSize(executorPoolSize+1);
            threadPoolTaskExecutor.setMaxPoolSize(executorPoolSize+1);
        }

        logPoolSize();
    }

    public void decreasePoolSize(){
        int executorPoolSize = threadPoolTaskExecutor.getCorePoolSize();

        if (executorPoolSize > 1){
            threadPoolTaskExecutor.setCorePoolSize(executorPoolSize-1);
            threadPoolTaskExecutor.setMaxPoolSize(executorPoolSize-1);
        }

        logPoolSize();
    }

    private void logPoolSize(){
        logger.info(String.format("ThreadPoolTaskExecutor '%s' - Pool Size: %d", threadPoolTaskExecutor.getThreadNamePrefix(), threadPoolTaskExecutor.getCorePoolSize()));
    }
}
