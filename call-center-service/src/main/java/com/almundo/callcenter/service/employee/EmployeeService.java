package com.almundo.callcenter.service.employee;

import com.almundo.callcenter.domain.model.employee.Employee;

import java.util.List;

public interface EmployeeService {
    /**
     * Inserta un empleado a la lista de empleados disponible para atender una llamada.
     * @param employee empleado.
     */
    void login(Employee employee);

    /**
     * Elimina un empleado de la lista de empleados disponible para atender una llamada.
     * @param employeeId id de empleado.
     */
    void logout(int employeeId);

    /**
     * Obtiene un empleado de menor rango que esté disponible para atender una llamada
     */
    Employee getOnlineEmployee();

    /**
     * Obtiene todos los empleados que iniciaron sesión.
     */
    List<Employee> getAllEmployees();
}
