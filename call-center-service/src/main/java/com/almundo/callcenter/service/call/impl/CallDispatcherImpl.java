package com.almundo.callcenter.service.call.impl;

import com.almundo.callcenter.domain.model.Call;
import com.almundo.callcenter.domain.model.CallResult;
import com.almundo.callcenter.domain.model.employee.Employee;
import com.almundo.callcenter.domain.model.employee.EmployeeStatus;
import com.almundo.callcenter.service.call.CallDispatcher;
import com.almundo.callcenter.service.dto.CallProcessResultDto;
import com.almundo.callcenter.service.employee.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.ReentrantLock;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallDispatcherImpl implements CallDispatcher {

    private ReentrantLock lock;

    @Autowired
    EmployeeService employeeService;

    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    private final int CALL_TIME_MIN = 5000;
    private final int CALL_TIME_MAX = 10001;

    public CallDispatcherImpl(){
        lock = new ReentrantLock();
    }

    @Async("callsExecutor")
    @Override
    public Future<CallProcessResultDto> dispatchCall(Call call) throws InterruptedException {
        logger.info(String.format("* Start Processing Call - Customer:%s - Thread id:%d", call.getCustomer().getName() ,Thread.currentThread().getId()));

        int callTime = ThreadLocalRandom.current().nextInt(CALL_TIME_MIN, CALL_TIME_MAX);

        assignEmployee(call);

        logger.info(String.format("* Assigned Employee:%s / %s - Thread id:%d", call.getEmployee().getName(), call.getEmployee().getType() ,Thread.currentThread().getId()));

        // Sleep between 5s and 10s for simulating the processing
        Thread.sleep(callTime);

        double callSeconds = callTime/1000;
        String processInfo = String.format("*** Processing is Done - Customer:%s - Call time=%.2f - Thread id=%d", call.getCustomer().getName(), callSeconds, Thread.currentThread().getId());

        call.setCallTime(callSeconds);

        employeeRelease(call);

        CallProcessResultDto resultDto = new CallProcessResultDto();
        resultDto.setCallResult(CallResult.OK);
        resultDto.setResultMessage(processInfo);

        return new AsyncResult(resultDto);
    }

    private void assignEmployee(Call call){
        lock.lock();
        try {
            Employee employee = employeeService.getOnlineEmployee();

            call.setEmployee(employee);
        } finally {
            lock.unlock();
        }
    }

    private void employeeRelease(Call call){
        lock.lock();
        try {
            Employee employee = call.getEmployee();
            employee.setStatus(EmployeeStatus.ONLINE);

        } finally {
            lock.unlock();
        }
    }
}
