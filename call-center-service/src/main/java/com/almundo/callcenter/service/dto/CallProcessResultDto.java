package com.almundo.callcenter.service.dto;

import com.almundo.callcenter.domain.model.CallResult;

public class CallProcessResultDto {
    private CallResult callResult;
    private String resultMessage;

    public CallResult getCallResult() {
        return callResult;
    }

    public void setCallResult(CallResult callResult) {
        this.callResult = callResult;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }
}
