package com.almundo.callcenter.service.call;

import com.almundo.callcenter.domain.model.Call;
import com.almundo.callcenter.service.dto.CallProcessResultDto;

import java.util.concurrent.Future;

public interface CallDispatcher {
    /**
     * Procesa la llamada con un empleado activo.
     * @param call llamada a procesar.
     */
    Future<CallProcessResultDto> dispatchCall(Call call) throws InterruptedException;
}
