package com.almundo.callcenter.service.employee.impl;

import com.almundo.callcenter.domain.model.employee.Employee;
import com.almundo.callcenter.domain.model.employee.EmployeeStatus;
import com.almundo.callcenter.repository.employee.EmployeeRepository;
import com.almundo.callcenter.service.employee.EmployeeService;
import com.almundo.callcenter.service.threading.CallsThreadPoolConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    CallsThreadPoolConfigurator callsThreadPoolConfigurator;

    @Override
    public void login(Employee employee) {
        employeeRepository.addEmployee(employee);

        // Cuando un empleado inicia sesión incremeto la cantidad de threads disponible para procesar las llamadas
        // Manteniendo como maximo de threads la cantidad indicada en la configuración {calls.thread.max-pool}
        callsThreadPoolConfigurator.increasePoolSize();

        logger.info(String.format("The employee '%s' has logged in", employee.getName()));
    }

    @Override
    public void logout(int employeeId) {
        employeeRepository.deleteEmployee(employeeId);

        // Cuando un empleado cierra sesión decremento la cantidad de threads disponible para procesar las llamadas
        callsThreadPoolConfigurator.decreasePoolSize();

        logger.info(String.format("The employee ID:%d has logged out", employeeId));
    }

    @Override
    public Employee getOnlineEmployee() {
        List<Employee> employees = employeeRepository.getEmployees();

        List<Employee> onlineEmployees = employees.stream()
                                                    .filter(x -> EmployeeStatus.ONLINE.equals(x.getStatus()))
                                                    .collect(Collectors.toList());
        onlineEmployees.sort(Comparator.comparingInt(Employee::getRank));

        Employee employee = onlineEmployees.size() > 0 ? onlineEmployees.get(0) : null;

        if(employee != null)
            employee.setStatus(EmployeeStatus.BUSY);

        return employee;
    }

    @Override
    public List<Employee> getAllEmployees() {
        return employeeRepository.getEmployees();
    }
}
