package com.almundo.callcenter.domain.model;

import com.almundo.callcenter.domain.model.employee.Employee;

public class Call {
    private Customer customer;
    private Employee employee;
    private double callTime;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public double getCallTime() {
        return callTime;
    }

    public void setCallTime(double callTime) {
        this.callTime = callTime;
    }
}
