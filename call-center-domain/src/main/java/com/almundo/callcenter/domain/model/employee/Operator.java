package com.almundo.callcenter.domain.model.employee;

public class Operator extends Employee{

    private final int RANK = 1;

    @Override
    public int getRank() {
        return RANK;
    }
}
