package com.almundo.callcenter.domain.model.employee;

import com.almundo.callcenter.domain.model.Person;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Director.class),
        @JsonSubTypes.Type(value = Supervisor.class),
        @JsonSubTypes.Type(value = Operator.class),
})
public abstract class Employee extends Person {
    private int id;

    private EmployeeStatus status;

    public abstract int getRank();

    public String getType() {
        return this.getClass().getSimpleName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EmployeeStatus getStatus() {
        return status;
    }

    public void setStatus(EmployeeStatus status) {
        this.status = status;
    }
}
