package com.almundo.callcenter.domain.model.employee;

public class Director extends Employee{

    private final int RANK = 3;

    @Override
    public int getRank() {
        return RANK;
    }
}