package com.almundo.callcenter.domain.model.employee;

public enum EmployeeStatus {
    ONLINE("ONLINE"),
    BUSY("BUSY");

    private String code;

    private EmployeeStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }
}
