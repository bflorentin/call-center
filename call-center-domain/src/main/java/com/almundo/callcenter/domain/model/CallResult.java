package com.almundo.callcenter.domain.model;

public enum CallResult {
    OK("OK"),
    ERROR("ERROR");

    private String code;

    private CallResult(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }
}
