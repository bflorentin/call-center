package com.almundo.callcenter.domain.model.employee;

public class Supervisor extends Employee{

    private final int RANK = 2;

    @Override
    public int getRank() {
        return RANK;
    }
}